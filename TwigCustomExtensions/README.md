## Extensiones Twig personalizadas
[Volver al README principal](../README.md)

Extensiones para Twig personalizadas:

- **slug** : convierte en slug un texto. Varias configuraciones

---
---


### Configuración: Inscribir el servicio.

En el archivo `./config/services.yaml` ...

```
service:
    didweb.myresources.twigcustomextensions:
          public: true
          autowire: true
          class: DidWeb\Bundle\TwigCustomExtensions\DidWebExtension
          tags:
              - { name: twig.extension }
```

---
---


### Extensiones:

#### Slug

Uso:

`{{ 'á é í ó  ú'|slug }}`

Devuelve:

`a-e-i-o-u`

##### Valores predeterminados

* 'delimiter'   => '-'
* 'limit'       => null
* 'lowercase'   => true
* 'replacements'  => array()
* 'transliterate' => true

...

Por ejemplo para cambiar el *delimiter*...

`{{ 'á é í ó ú'|slug({ 'delimiter': '#'}) }}`

Devuelve:

 `a#e#i#o#u`


 ---

 [Volver al README principal](../README.md)
