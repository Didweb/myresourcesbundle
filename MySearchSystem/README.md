## My Search System

[Volver al README principal](../README.md)

Sistema para la creación de filtros utilizado en el backend.


Utilización en el controlador, ejemplo de uso...

```

use DidWeb\Bundle\MySearchSystem\MySearchSystem;


class NameController extends AbstractController
{
    public $mySearchSystem;

    public function __construct()
    {

      $allFilters = ['Por nombres'=>'by_names'];
      $this->mySearchSystem = new MySearchSystem($allFilters, 'person_search_web');

    }

    /**
     * @Route("/", name="app_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
      $allPerson =  $personRepository->findAll();
      $allPerson = $this->allFilters($allPerson, $request);

      $this->mySearchSystem->updateStatus($request);

      return $this->render('index.html.twig', [
            'people' => $allPerson,
            'mySearchSystem' => $this->mySearchSystem,
            'formSearch' => $formSearch->createView(),
        ]);
    }


    public function allFilters($allElements, $request)
    {
          // Filtrado por nombre y apellidos
          $resultFilter = $this->mySearchSystem->filterByNames($allElements, $request,'by_names',
                                                ['getName', 'getSurnameFirst', 'getSurnameSecond']);

          return $resultFilter;
        }


    /**
    * @Route("/limpiar-filtro", name="person_admin_clean_filter")
    */
    public function cleanFilters(Request $request)
    {

      $this->mySearchSystem->cleanFilters($request);
      return $this->redirectToRoute('person_index');
    }
}

```

---

Funciones especiales de **MySearchSystem**:

* **allFilters**: Aqui vamos añadiendo uno detrás  de otro los filtro a que se deben aplicar.
* **cleanFilters**: Para limpiar los filtros, eliminamos el filtro aplicado del listado.

[Documentación completa en GitBook.com ](https://eduardo-2.gitbook.io/mysearchsystem/)


---

[Volver al README principal](../README.md)
