<?php
namespace DidWeb\Bundle\MySearchSystem;

use Symfony\Component\HttpFoundation\Request;
use DidWeb\Bundle\DidClasses\Slugs;

class MySearchSystem
{
  private $allFiltres = [];

  private $nameForm;
  public $hasFilters;
  public $filtersActives;

  public function __construct($allFilters, $nameForm) {
    $this->allFiltres = $allFilters;
    $this->nameForm = $nameForm;
  }

  public function getAllFilter() {
    return $this->allFiltres;
  }

  public function updateStatus($request) {
    try {
      $this->withoutFilters($request);
      $this->titlesFiltersActive($request);
    }   catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return true;
  }

  public function withoutFilters($request) {

      $session = $request->getSession();
      $total = count($this->allFiltres);
      $n = 0;
      foreach($this->allFiltres as $nameSession) {

        if($session->get($nameSession) == 'all'
          || $session->get($nameSession) === null ) {
          $n++;
        }
      }
      $this->hasFilters = ($n == 0) ? true: false;

      return $this->hasFilters;
    }


  public function cleanFilters($request) {
    $session = $request->getSession();
    foreach($this->allFiltres as $filter) {
      $session->remove($filter);
    }

  }

  public function filterStatus($session, $request, $nameFilter) {
    $filterStatus = null;

      if(($session->get($nameFilter) != null || $session->get($nameFilter) != "")
        && (isset($request->get($this->nameForm)[$nameFilter]) == false
            || $request->get($this->nameForm)[$nameFilter] == '')) {
          $filterStatus = $session->get($nameFilter);


        } else {
          if (isset($request->get($this->nameForm)[$nameFilter]) != false
              && $request->get($this->nameForm)[$nameFilter] != '') {

                $filterStatus = $request->get($this->nameForm)[$nameFilter];
                $session->set($nameFilter, $filterStatus);

              }
        }


      return $filterStatus;
  }


  public function titlesFiltersActive($request) {

    $session = $request->getSession();
    foreach($this->allFiltres as $keyName => $nameSession) {
    if($session->get($nameSession) !=''
          && $session->get($nameSession) !='all') {
        $this->filtersActives[$keyName] = $session->get($nameSession);
      } else {
        $this->filtersActives[$keyName] = 'Todos';
      }
    }
    return $this->filtersActives;
  }


  public function filterByNames($allElements, $request, $nameFilter, $feldsToSearch) {

    $session = $request->getSession();
    $filterStatus = $this->filterStatus($session, $request, $nameFilter);
    $allWithlFilter = [];
    $slugs = new Slugs();

    if($filterStatus !== null && $filterStatus !== 'all') {

      foreach($allElements as $element) {

        foreach($feldsToSearch as $checkFeld) {
          $checkFeld = $element->$checkFeld();

          $slugCheckFeld = (string)$checkFeld;
          $slugFilterStatus = $filterStatus;
          $slugCheckFeld    =  $slugs->slugUrlsAction($slugCheckFeld, ['transliterate'=>true]);
          $slugFilterStatus =  $slugs->slugUrlsAction($slugFilterStatus, ['transliterate'=>true]);
          $pos = strpos(strtolower($slugCheckFeld), strtolower($slugFilterStatus));
          if($pos !== false){

            $allWithlFilter[$element->getId()] = $element;
          }

        }

      }

      $allElements = $allWithlFilter;
    }
    return $allElements;
  }

}
