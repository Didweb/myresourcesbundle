## Control Menus

[Volver al README principal](../README.md)

Esta función se encarga de enviar la clase (CSS) para poder identificar si el controlador necesita activar el menú o no.

Se han de definir las opciones de menú sobre las que se quiere intervenir.

Así como el nombre de la clase (CSS) que determinara si esta activo o no.

### Configuration
---

Definimos los parámetros:

- **controlMenusActives** = array con el nombre de opciones del menú. Estos serán usados para identificar en el html.
- **controlMenusClass** = Nombre de la clase (CSS) que determina si esta activado.



```
// ./config/services.yaml

parameters:
    controlMenusActives: ['option_1', 'option_2', 'option_3', 'option_4']
    controlMenusClass: 'active'

```


---
---


### Use
---

In Controller...

```
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use DidWeb\Bundle\ControlMenus\ControlMenus;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $controlMenus = new ControlMenus();

        return $this->render('home/index.html.twig', [
            'controlMenus' => $controlMenus->activesMenu('menu 3')
        ]);
    }
}

```

in template...

`{{ controlMenus['option_1'] }}` = Nos devolverá el nombre de la clase, definida en *controlMenusClass*

```
<ul>
  <li><a href='#' class="{{ controlMenus['option_1'] }}">Option Menu 1 {{ controlMenus['option_1'] }}</a></li>
  <li><a href='#' class="{{ controlMenus['option_2'] }}">Option Menu 2 {{ controlMenus['option_2'] }}</a></li>
  <li><a href='#' class="{{ controlMenus['option_3'] }}">Option Menu 3 {{ controlMenus['option_3'] }}</a></li>
  <li><a href='#' class="{{ controlMenus['option_4'] }}">Option Menu 4 {{ controlMenus['option_4'] }}</a></li>
  <ul>
```


---

[Volver al README principal](../README.md)
