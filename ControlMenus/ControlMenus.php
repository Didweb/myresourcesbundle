<?php

namespace DidWeb\Bundle\ControlMenus;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ControlMenus extends AbstractController
{


  private $actives;
  private $class;

  public function __construct() {

    global $kernel;
    $cont = $kernel->getContainer();

    $activesBrut = $cont->getParameter('controlMenusActives');
    foreach($activesBrut as $optionsMenu) {
      $this->actives[$optionsMenu] = ' ';
    }
    $this->class   = $cont->getParameter('controlMenusClass');
  }


  public  function activesMenu($secctionActive) {

    if (array_key_exists($secctionActive, $this->actives)) {

      foreach($this->actives as $key=>$secction) {
        if($secction != ' ') {
          $this->actives[$secction] = ' ';
        }

      }
      $this->actives[$secctionActive] = $this->class;

      return $this->actives;
    }

    foreach($this->actives as $key=>$secction) {
        $this->actives[$key] = 'Error. The option: '.$secctionActive.'  not exist!';
    }
    return $this->actives;
  }

  public function getActives() {
      return $this->actives;
  }
}
