# My Resources Bundle

Bundle con los recursos auxiliares para la creación de aplicaciones en Symfony.

Suelen ser rutinas y pequeños funciones que utilizo con frecuencia y reutilizo en los distintos proyectos.

## Recursos existentes

* **ControlMenus**: Devuelve la clase CSS si el menú esta activo o no. [Más info: Config y uso](./ControlMenus/README.md)
* **MySearchSystem**: Sistema buscador devuelve listado filtrado según filtro aplicado. [Documentación completa en GitBook.com ](https://eduardo-2.gitbook.io/mysearchsystem/)
* **TwigCustomExtensions**: Extensiones Twig para facilitar tareas en el front. [Más info: Config y uso](./TwigCustomExtensions/README.md)
* **Slug**: Creación de slugs. (Autor:  Sean Murphy) [Código en Github](https://gist.github.com/sgmurphy/3098978)


## Instalación

Se ha de declarar el servicio para las extensiones Twig.

En el archivo `./config/services.yaml` ...

```
service:
    didweb.myresources.twigcustomextensions:
          public: true
          autowire: true
          class: DidWeb\Bundle\TwigCustomExtensions\DidWebExtension
          tags:
              - { name: twig.extension }
```
